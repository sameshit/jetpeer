var WebSocketServer = require('ws').Server
  , wss = new WebSocketServer({ port: 9990 });
var bunyan = require('bunyan');
var logger = bunyan.createLogger({name: 'server.js'});

function tryParse(msg){
	var json;
	try {json = JSON.parse(msg);}
	catch(e){return false;}
	finally{return json;}
}

var swarm = {};
var lastConnectionId = 0;

wss.on('connection', function connection(ws) {
  ws.connectionId = ++lastConnectionId;
  var peer = swarm[lastConnectionId] = ws;

  ws.on('message', function incoming(message) {
    var json = tryParse(msg);
    if (!json){
      logger.warn("invalid meesage: "+message);
      connection.close();
    }

    switch(json.cmd){
      case "register":
        peer.sdp = json.params.sdp;
        
      break;      
    }
  });
  
});

