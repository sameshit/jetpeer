var	utils = {};

utils.objectSize = function(obj){
	var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

utils.getBe16 = function(arr,pos){
	var result = arr[pos] << 8;
	result |= arr[pos+1];
	return result;
}

utils.getBe32 = function(arr,pos){
	var result;
    result  = utils.getBe16(arr,pos) << 16;
    result |= utils.getBe16(arr,pos+2);
    return result;
}

utils.putBe16 = function(arr,pos,val){
	if (val < 0)
		throw new Error("Only positive numbers are allowed");	
	arr[pos]   = (val >> 8)&255;
	arr[pos+1] = val&255;
	return pos;
}

utils.putBe32 = function(arr,pos,val){
	if (val < 0)
		throw new Error("Only positive numbers are allowed");	
	arr[pos]   = (val >> 24)&255;
	arr[pos+1] = (val >> 16)&255;
	arr[pos+2] = (val >> 8)&255;
	arr[pos+3] = val&255;
	return pos;
}


utils.getPackedIntegerSize= function(val){
	return Math.ceil(((val).toString(2).length+1)/7);	

}

utils.packUnsignedInteger = function(arr,pos,val,isfull){
	if (val < 0)
		throw new Error("Only positive numbers are allowed");
	var len  = utils.getPackedIntegerSize(val);	
	isfull = typeof isfull !== 'undefined' ? isfull : false;

	for (var i = pos ; i < pos+len; ++i){
		if (i+1==pos+len)			
			arr[i] = (1<<7);	
		else
			arr[i] = 0;		
		if (i == pos){			
			if (isfull)
				arr[i] |= (1<<6);						
			arr[i] |= (val)&63;						
		}
		else{
			arr[i] |= (val >> (6+7*(i-pos-1)))&127;			
		}
	}
	return pos + len;
}

utils.unpackUnsignedInteger = function(arr,pos){
	var val = 0;
	var skip;
	var i = pos;
	var isfull;

	do {
		if (i == pos){
			isfull = ((arr[i]&(1<<6)) == (1<<6));
			val |= ((arr[i] & ~(1<<7) & ~(1<<6)) << (6*(i-pos))); 			
		}
		else
			val |= ((arr[i] & ~(1<<7)) << (6+7*(i-pos-1))); 					
		skip = ((arr[i]&(1<<7)) == (1<<7));				
		++i;
		if (i - pos > 6)
			throw new Error("It seems that we run into infinite loop...");
	}
	while(!skip);
	return {
		pos: i,
		value: val,
		isFullPacket: isfull
	};
}

utils.arrayBufferToString = function(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}

utils.stringToArrayBuffer = function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i<strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

this.utils=utils;