var jetpeer = require('../build/jetpeer.js');
var jp = new jetpeer();

var assert = require('assert');

describe('utils',function(){
	describe('integer packing',function(){
		it('should correctly pack and unpack integers with zero starting pos',function(){
			var testValues = [0,1,2,3,10,15,56,62,63,64,65,126,127,128,511,512,513,1021,1022,1023,1024,1025,1026,65000,100000,5678901];
			var arr = new ArrayBuffer(100);
			var uarr = new Uint8Array(arr);

			for (var i in testValues){
				jp.utils.packUnsignedInteger(uarr,0,testValues[i]);
				assert.equal(testValues[i],jp.utils.unpackUnsignedInteger(uarr,0).value);
			}
		});
	});

	describe('string packing',function(){
		it('should correctly convert string2arraybuffer and arraybuffer2string',function(){
			assert.equal('hello world',jp.utils.arrayBufferToString(jp.utils.stringToArrayBuffer('hello world')));
		});
	});
});