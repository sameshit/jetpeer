var jetpeer = require('../../build/jetpeer.js');
var assert = require('assert');

describe('live muxer and demuxer',function(){
	describe('packet packing',function(){
		it("should correctly pack and unpack simple packets",function(){
			var jp = new jetpeer();
			var muxer = new jp.liveMuxer();
			var demuxer = new jp.liveDemuxer();
			var streamId, str = "hello world",arr;

			demuxer.events.on('packet',function(stream,packet){
				assert.equal(stream.streamId,streamId);
				assert.equal(str,jp.utils.arrayBufferToString(packet));
			});

			streamId = muxer.createStream(jp.codecs.opus.id,{bitRate:1200,sampleRate:900,channels:2});
			arr = muxer.__serializeHeader__();
			demuxer.__parseHeader__(new Uint8Array(arr,1));
			arr = muxer.__serializeMedia__(streamId,jp.utils.stringToArrayBuffer(str));			
			demuxer.__parseMedia__(new Uint8Array(arr,1));
		});

		it("should correctly pack and unpack big packets",function(){
			var jp = new jetpeer();
			var muxer = new jp.liveMuxer();
			var demuxer = new jp.liveDemuxer();
			var streamId,arr;
			var packet = new ArrayBuffer(64*1024);
			var upacket = new Uint8Array(packet);

			for (var i = 0; i < upacket.byteLength; ++i)
				upacket[i] = i %255;

			demuxer.events.on('packet',function(stream,packet){
				assert.equal(stream.streamId,streamId);
				var uupacket = new Uint8Array(packet);
				assert.equal(upacket.byteLength,uupacket.byteLength);
				for (var i = 0; i < uupacket.byteLength; ++i)
					assert.equal(i%255,uupacket[i]);
			});

			streamId = muxer.createStream(jp.codecs.opus.id,{bitRate:1200,sampleRate:900,channels:2});
			arr = muxer.__serializeHeader__();
			demuxer.__parseHeader__(new Uint8Array(arr,1));
			arr = muxer.__serializeMedia__(streamId,packet);			
			demuxer.__parseMedia__(new Uint8Array(arr,1));
		});		
	});
});