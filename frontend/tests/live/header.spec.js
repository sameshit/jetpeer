var jetpeer = require('../../build/jetpeer.js');
var assert = require('assert');

describe('live muxer and demuxer',function(){
	describe('header packing',function(){
		it("should correctly pack and unpack video header",function(){
			var jp = new jetpeer();
			var muxer = new jp.liveMuxer();
			var demuxer = new jp.liveDemuxer();
			var stream_id;

			demuxer.events.on('stream',function(stream){
				assert.equal(stream.width,1200);
				assert.equal(stream.height,900);
				assert.equal(stream.streamId,stream_id);
			});
			stream_id = muxer.createStream(jp.codecs.vp8.id,{width:1200,height:900});
			var arr = muxer.__serializeHeader__();
			demuxer.__parseHeader__(new Uint8Array(arr,1));
			var uarr = new Uint8Array(arr,0,1);			
			var pktType = jp.utils.unpackUnsignedInteger(uarr,0).value;
			assert.equal(jp.packetTypes.header,pktType);
		});
		it("should correctly pack and unpack audio header",function(){
			var jp = new jetpeer();
			var muxer = new jp.liveMuxer();
			var demuxer = new jp.liveDemuxer();
			var stream_id;

			demuxer.events.on('stream',function(stream){
				assert.equal(stream.bitRate,1200);
				assert.equal(stream.sampleRate,900);
				assert.equal(stream.channels,2);
				assert.equal(stream.streamId,stream_id);
			});
			stream_id = muxer.createStream(jp.codecs.opus.id,{bitRate:1200,sampleRate:900,channels:2});
			var arr = muxer.__serializeHeader__();
			demuxer.__parseHeader__(new Uint8Array(arr,1));		
		});
		it("should correctly pack and unpack 2 headers",function(){
			var jp = new jetpeer();
			var muxer = new jp.liveMuxer();
			var demuxer = new jp.liveDemuxer();
			var streamId1,streamId2,curStream;

			demuxer.events.on('stream',function(stream){			
				if (curStream == streamId1){
					assert.equal(stream.bitRate,1200);
					assert.equal(stream.sampleRate,900);
					assert.equal(stream.channels,2);
					assert.equal(stream.streamId,streamId1);
					curStream = streamId2;
				}
				else{
					assert.equal(stream.width,1200);
					assert.equal(stream.height,900);
					assert.equal(stream.streamId,streamId2);					
				}
			});
			streamId1 = muxer.createStream(jp.codecs.opus.id,{bitRate:1200,sampleRate:900,channels:2});
			streamId2 = muxer.createStream(jp.codecs.vp8.id,{width:1200,height:900});
			curStream = streamId1;
			var arr = muxer.__serializeHeader__();
			demuxer.__parseHeader__(new Uint8Array(arr,1));		
		});		
	});
});