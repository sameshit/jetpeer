var jetpeer = require('../../build/jetpeer.js');
var assert = require('assert');

describe('live muxer and demuxer',function(){
	describe('frame packing',function(){
		it('should pack and unpack simple frames with flushes',function(){
			var jp = new jetpeer();
			var muxer = new jp.liveMuxer();
			var demuxer = new jp.liveDemuxer();
			var str = "hello world",streamId;
			var called = false;

			muxer.events.on('frame',function(frame){
				demuxer.parse(frame);
				called = true;
			});

			demuxer.events.on('packet',function(stream,packet){
				assert.equal(stream.streamId,streamId);
				assert(str,jp.utils.arrayBufferToString(packet));
			});

			streamId = muxer.createStream(jp.codecs.opus.id,{bitRate:1200,sampleRate:900,channels:2});
			muxer.writePacket(streamId,jp.utils.stringToArrayBuffer(str));
			muxer.flush();
			assert.equal(called,true);
		});
		it('should pack and unpack big frames',function(){
			var jp = new jetpeer();
			var muxer = new jp.liveMuxer();
			var demuxer = new jp.liveDemuxer();
			var streamId;
			var packet = new ArrayBuffer(1290);
			var upacket = new Uint8Array(packet);

			for (var i = 0; i < upacket.length; ++i)
				upacket[i] = i %255;			

			muxer.events.on('frame',function(frame){												
				demuxer.parse(frame);
			});

			demuxer.events.on('packet',function(stream,packet){								
				assert.equal(stream.streamId,streamId);
				var uupacket = new Uint8Array(packet);	
				console.log(1);
				assert.equal(upacket.length,uupacket.length);
				for (var i = 0; i < uupacket.length; ++i){					
					assert.equal(i%255,uupacket[i]);
				}
			});

			streamId = muxer.createStream(jp.codecs.opus.id,{bitRate:1200,sampleRate:900,channels:2});
			muxer.writePacket(streamId,packet);
			muxer.flush();
		});
		// it('should be able to parse incomplete frames',function(){
		// 	var jp = new jetpeer();
		// 	var muxer = new jp.liveMuxer();
		// 	var demuxer = new jp.liveDemuxer();
		// 	var str = "hello world",streamId;
		// 	var frameNum = 1;
		// 	var called = false;

		// 	muxer.events.on('frame',function(frame){
		// 		assert.equal(frameNum<3,true);
		// 		if (frameNum == 2)
		// 			demuxer.parse(frame);
		// 		++frameNum;
		// 	});

		// 	demuxer.events.on('packet',function(stream_id,packet){
		// 		called = true;
		// 		assert.equal(stream_id,streamId);
		// 		console.log(jp.utils.ArrayBufferToString(packet));
		// 		assert(str,jp.utils.ArrayBufferToString(packet));				
		// 	});

		// 	streamId = muxer.createStream(jp.codecs.opus.id,{bitRate:1200,sampleRate:900,channels:2});
		// 	muxer.writePacket(streamId,new ArrayBuffer(1325));
		// 	muxer.writePacket(streamId,jp.utils.stringToArrayBuffer(str));
		// 	muxer.flush();
		// 	assert.equal(called,true);
		// });
	});
});