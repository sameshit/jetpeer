jetpeer.decodersFactory = function() {
	var obj = {};	

	obj.create = function(params){
		switch (params.name){
			case "opus":
				return new jetpeer.decoders.opusDecoder(params);
			break;
			case "vp8":
				return new jetpeer.decoders.vp8Decoder(params);
			break;
			default:
				throw new Error("Unsupported codec name: "+params.name);
		}
		
	}

	return obj;
}