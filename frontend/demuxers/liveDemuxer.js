var liveDemuxer = function(params){
	baseDemuxer.call(this);	

	var header_parsed = false;
	var that = this;
	var packet = false;
	var packetPos;	

	this.parse = function(aframe){
		var frame = new Uint8Array(aframe);
		if (frame.length != FRAME_SIZE)
			throw new Error("Invalid frame size: "+frame.length);

		var framePos = 0;		
		while (framePos < FRAME_SIZE){			
			var result 		= utils.unpackUnsignedInteger(frame,framePos);
			framePos 		= result.pos;
			var packetSize 	= result.value;
			var isFull		= result.isFullPacket;

			if (!packet && !isFull)
				framePos += packetSize;									
			else if (packetSize == 0){
				// skip empty packets				
			}
			else{				
				if (!packet){					
					packet = new ArrayBuffer(packetSize);
					packetPos = 0;
				}				
				var sizeToCopy = FRAME_SIZE - framePos < packetSize ? FRAME_SIZE-framePos : packetSize;
				var uarr = new Uint8Array(packet,packetPos,sizeToCopy);
				var uframe = new Uint8Array(aframe,framePos,sizeToCopy);
				uarr.set(uframe);
				packetPos += sizeToCopy;				
				framePos += sizeToCopy;					
				if (packetPos > packet.byteLength)
					throw new Error("Invalid packet pos: "+packetPos);
				if (packetPos == packet.byteLength){					
					that.__parsePacket__(packet);
					packet =false;
				}
			}
		}
	}	

	this.__parsePacket__ = function(packet){		
		var uarr = new Uint8Array(packet);
		var result;

		result = utils.unpackUnsignedInteger(uarr,0);				
		switch (result.value){
			case PACKET_HEADER:
				that.__parseHeader__(new Uint8Array(packet,result.pos));
			break;
			case PACKET_MEDIA:
				that.__parseMedia__(new Uint8Array(packet,result.pos));
			break;
			default:
				throw new Error("Unknown packet type: "+result.value);
			break;
		}
	}

	function checkPos(pos,uarr){
		if (pos > uarr.length)
			throw new Error("Invalid pos while parsing: "+pos);
	}

	this.__parseHeader__ = function(uarr){		
		var header = {};
		var result,pos = 0,stream;

		while (pos < uarr.length){
			result = utils.unpackUnsignedInteger(uarr,pos);			
			stream = that.streams[result.value] = {};
			stream.streamId = result.value;			
			checkPos(result.pos,uarr);
			result = utils.unpackUnsignedInteger(uarr,result.pos);
			stream.codec = result.value;
			checkPos(result.pos,uarr);

			switch (stream.codec){
				case CODEC_VP8:
				case CODEC_JPEG:
					result = utils.unpackUnsignedInteger(uarr,result.pos);
					stream.width = result.value;
					checkPos(result.pos,uarr);
					result = utils.unpackUnsignedInteger(uarr,result.pos);
					stream.height = result.value;
					checkPos(result.pos,uarr);
				break;
				case CODEC_WAV:
				case CODEC_OPUS:
					result = utils.unpackUnsignedInteger(uarr,result.pos);
					stream.sampleRate = result.value;
					checkPos(result.pos,uarr);
					result = utils.unpackUnsignedInteger(uarr,result.pos);
					stream.bitRate = result.value;
					checkPos(result.pos,uarr);
					result = utils.unpackUnsignedInteger(uarr,result.pos);
					stream.channels = result.value;
					checkPos(result.pos,uarr);
				break;
			}
			pos = result.pos;
			that.events.emitEvent('stream',[stream]);
		}
	}

	this.__parseMedia__= function(uarr){
		var result,stream,packet,upacket;

		result = utils.unpackUnsignedInteger(uarr,0);
		if (typeof that.streams[result.value] === 'undefined')
			throw new Error("Invalid stream id while parsing packet: "+result.value);
		stream = that.streams[result.value];
		checkPos(result.pos,uarr);		

		packet = new ArrayBuffer(uarr.length-result.pos);		
		upacket = new Uint8Array(packet);
		upacket.set(uarr.slice(result.pos));

		that.events.emitEvent('packet',[stream,packet]);
	}
}

liveDemuxer.prototype = Object.create(baseDemuxer);
liveDemuxer.prototype.constructor = liveDemuxer;
this.liveDemuxer = liveDemuxer;