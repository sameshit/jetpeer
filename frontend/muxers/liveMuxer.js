function liveMuxer(params){	
	baseMuxer.call(this);
	if (typeof params !== 'object')
		params = {};

	if (!params.hasOwnProperty("headerFrequency"))
		params.headerFrequency = Number.MAX_VALUE;
	if (typeof ArrayBuffer === 'undefined')
		throw new Error("This environment doesn't support ArrayBuffer");
	if (typeof Uint8Array === 'undefined')
		throw new Error("This environment doesn't support Uint8Array");

	var lastStreamId = 0;
	var that = this;
	this.createStream = function(codec,params){		
		var stream = {};		
		switch (codec){
			case CODEC_VP8:
			case CODEC_JPEG:
				if (!params.hasOwnProperty("width") || !params.hasOwnProperty("height"))
					throw new Error("width or height parameters haven't been provided");
				stream.width = params.width;
				stream.height = params.height;
			break;
			case CODEC_WAV:
			case CODEC_OPUS:
				if (!params.hasOwnProperty("bitRate") || !params.hasOwnProperty("sampleRate") || !params.hasOwnProperty("channels"))
					throw new Error("bitRate, sampleRate or channels parameters haven't been provided");
				stream.bitRate = params.bitRate;
				stream.sampleRate = params.sampleRate;
				stream.channels = params.channels;
			break;
			default:
				throw new Error("Unknown codec type: "+codec);			
		}							
		stream.codec = codec;
		stream.streamId = ++lastStreamId;
		that.streams[stream.streamId] = stream;				
		return stream.streamId;
	};

	this.__serializeMedia__ = function (streamId,packet){
		if (!that.streams.hasOwnProperty(streamId))
			throw new Error("Unknown stream id: "+streamId);

		var infoSize = utils.getPackedIntegerSize(PACKET_MEDIA)+utils.getPackedIntegerSize(streamId);
		var arr  = new ArrayBuffer(packet.byteLength + infoSize);
		var uarr1= new Uint8Array(arr,0,infoSize);
		var uarr2= new Uint8Array(arr,infoSize);
		
		var pos = utils.packUnsignedInteger(uarr1,0,PACKET_MEDIA);
		utils.packUnsignedInteger(uarr1,pos,streamId);
		uarr2.set(new Uint8Array(packet));		
		return arr;
	}

	this.__serializeHeader__ = function(){
		var size = 0;		
		for (var i in that.streams){			
			switch (that.streams[i].codec){
				case CODEC_VP8:
				case CODEC_JPEG:
					size += utils.getPackedIntegerSize(that.streams[i].streamId);
					size += utils.getPackedIntegerSize(that.streams[i].codec);
					size += utils.getPackedIntegerSize(that.streams[i].width);
					size += utils.getPackedIntegerSize(that.streams[i].height);
				break;
				case CODEC_WAV:
				case CODEC_OPUS:
					size += utils.getPackedIntegerSize(that.streams[i].streamId);
					size += utils.getPackedIntegerSize(that.streams[i].codec);
					size += utils.getPackedIntegerSize(that.streams[i].sampleRate);
					size += utils.getPackedIntegerSize(that.streams[i].bitRate);
					size += utils.getPackedIntegerSize(that.streams[i].channels);
				break;
			}
		}		
		var infoSize = utils.getPackedIntegerSize(PACKET_HEADER);
		var arr  = new ArrayBuffer(size+infoSize);		
		var uarr = new Uint8Array(arr);		
		var pos = utils.packUnsignedInteger(uarr,0,PACKET_HEADER);

		for (var i in that.streams){			
			switch (that.streams[i].codec){
				case CODEC_VP8:
				case CODEC_JPEG:
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].streamId);
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].codec);
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].width);
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].height);
				break;
				case CODEC_WAV:
				case CODEC_OPUS:
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].streamId);
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].codec);
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].sampleRate);
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].bitRate);
					pos = utils.packUnsignedInteger(uarr,pos,that.streams[i].channels);
				break;
			}
		}
		return arr;		
	}

	var framePos = 0;
	var packetCount = 0;
	var frameArray = new ArrayBuffer(FRAME_SIZE);
	var frameBytes = new Uint8Array(frameArray);

	this.__makeFrame__ = function (pkt){
		var remSizeSize = utils.getPackedIntegerSize(pkt.byteLength);
		while (remSizeSize+1 > FRAME_SIZE - framePos)
			framePos = utils.packUnsignedInteger(frameBytes,framePos,0); // Put null packets if it's impossible to store at least one byte of data in this frame
		if (framePos == FRAME_SIZE){
				that.events.emitEvent('frame',[frameArray]);
				framePos = 0;
		}

		var remSize = pkt.byteLength;
		while (remSize > 0){
			framePos = utils.packUnsignedInteger(frameBytes,framePos,remSize,remSize == pkt.byteLength);
			var sizeToPut = remSize < FRAME_SIZE-framePos ? remSize : FRAME_SIZE-framePos;
			frameBytes.subarray(framePos).set( new Uint8Array(pkt,pkt.length-remSize,sizeToPut));
			framePos += sizeToPut;
			remSize  -= sizeToPut;
			if (framePos == FRAME_SIZE){
				that.events.emitEvent('frame',[frameArray]);
				framePos = 0;
			}
		}
	}
		
	this.writePacket = function(streamId,packet){		
		if (packetCount % params.headerFrequency == 0)			
			that.__makeFrame__(that.__serializeHeader__());
		that.__makeFrame__(that.__serializeMedia__(streamId,packet));
		++packetCount;
	}

	this.flush = function(){
		while (framePos != FRAME_SIZE)
			framePos = utils.packUnsignedInteger(frameBytes,framePos,0);		
		that.events.emitEvent('frame',[frameArray]);		
	}
}

liveMuxer.prototype = Object.create(baseMuxer);
liveMuxer.prototype.constructor = liveMuxer;
this.liveMuxer = liveMuxer;