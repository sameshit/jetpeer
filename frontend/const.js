var codecs = {};
var packetTypes = {};

const CODEC_JPEG 	= 0;
const CODEC_WAV 	= 1;
const CODEC_VP8 	= 2;
const CODEC_OPUS 	= 3;

const PACKET_SKIP	= 0;
const PACKET_HEADER = 1;
const PACKET_MEDIA  = 2;

const STREAM_AUDIO	= 0;
const STREAM_VIDEO	= 1;

const FRAME_SIZE	= 1300;

codecs.jpeg = {};
codecs.jpeg.id = CODEC_JPEG;
codecs.wav = {};
codecs.wav.id = CODEC_WAV;
codecs.vp8 = {};
codecs.vp8.id = CODEC_VP8;
codecs.opus = {};
codecs.opus.id = CODEC_OPUS;

packetTypes.header = PACKET_HEADER;
packetTypes.skip   = PACKET_SKIP;
packetTypes.media  = PACKET_MEDIA;

this.codecs = codecs;
this.packetTypes = packetTypes;