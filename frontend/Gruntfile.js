module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			dist: {
			  src: ['3dparty/eventEmitter/EventEmitter.js',
			  'wrapper/begin.js','const.js','utils/*.js', 'demuxers/*.js','muxers/*.js',
			  'wrapper/end.js'],
			  dest: 'build/jetpeer.js'
			}
		},
		watch: {
		  scripts: {
		    files: ['**/*.js'],
		    tasks: ['concat'],
		    options: {
		      spawn: false,
		    },
		  },
		}	  
	});
	
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['concat']);
}